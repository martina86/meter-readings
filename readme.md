# REST API for meter readings and ratios



## Prerequisites

- JDK 1.8

- Maven 3



## Stack

- Spring Boot 2.0.3.RELEASE

- Spring Data JPA

- Spring Data REST 

- H2



## Run

`mvn clean spring-boot:run`



## API Doc & Sample



Documentation can be accessed after deployment at:



http://localhost:8080/swagger-ui.html



Documentation is generated with Swagger 2, and it describes following APIs:



- POST /ratios

- GET /ratios

- GET /ratios/{profile}

- PUT /ratios/{profile}

- DELETE /ratios/{profile}



- POST /readings

- GET /readings

- GET /readings/{meterId}

- PUT /readings/{meterId}

- DELETE /readings/{meterId}



- GET /consumption/{meterId}/{month}





### Sample for creating profiles with ratios 



 - POST /ratios  

 

 Body:

```json

{  

   "ratioList":[  

	  {  

		 "month":"JAN",

		 "profile":"A",

		 "ratio":0.05

	  },

	  {  

		 "month":"FEB",

		 "profile":"A",

		 "ratio":0.1

	  },

	  {  

		 "month":"MAR",

		 "profile":"A",

		 "ratio":0.05

	  },

	  {  

		 "month":"APR",

		 "profile":"A",

		 "ratio":0.1

	  },

	  {  

		 "month":"JUN",

		 "profile":"A",

		 "ratio":0.05

	  },

	  {  

		 "month":"JUL",

		 "profile":"A",

		 "ratio":0.1

	  },

	  {  

		 "month":"AUG",

		 "profile":"A",

		 "ratio":0.1

	  },

	  {  

		 "month":"SEP",

		 "profile":"A",

		 "ratio":0.05

	  },

	  {  

		 "month":"OCT",

		 "profile":"A",

		 "ratio":0.1

	  },

	  {  

		 "month":"NOV",

		 "profile":"A",

		 "ratio":0.1

	  },

	  {  

		 "month":"DEC",

		 "profile":"A",

		 "ratio":0.1

	  },

	  {  

		 "month":"MAY",

		 "profile":"A",

		 "ratio":0.1

	  }

   ]

}

```





### Sample for creating meter readings 





 - POST /readings  

 

 Body:

```json

 {  

   "meterReadings":[  

	  {  

		 "meterId":"0001",

		 "month":"DEC",

		 "profile":"A",

		 "reading":200

	  },

	  {  

		 "meterId":"0001",

		 "month":"JAN",

		 "profile":"A",

		 "reading":10

	  },

	  {  

		 "meterId":"0001",

		 "month":"FEB",

		 "profile":"A",

		 "reading":25

	  },

	  {  

		 "meterId":"0001",

		 "month":"MAR",

		 "profile":"A",

		 "reading":33

	  },

	  {  

		 "meterId":"0001",

		 "month":"APR",

		 "profile":"A",

		 "reading":55

	  },

	  {  

		 "meterId":"0001",

		 "month":"MAY",

		 "profile":"A",

		 "reading":77

	  },

	  {  

		 "meterId":"0001",

		 "month":"JUN",

		 "profile":"A",

		 "reading":88

	  },

	  {  

		 "meterId":"0001",

		 "month":"JUL",

		 "profile":"A",

		 "reading":110

	  },

	  {  

		 "meterId":"0001",

		 "month":"AUG",

		 "profile":"A",

		 "reading":130

	  },

	  {  

		 "meterId":"0001",

		 "month":"SEP",

		 "profile":"A",

		 "reading":140

	  },

	  {  

		 "meterId":"0001",

		 "month":"OCT",

		 "profile":"A",

		 "reading":160

	  },

	  {  

		 "meterId":"0001",

		 "month":"NOV",

		 "profile":"A",

		 "reading":180

	  }

   ]

}

```