package com.assignment.meter.readings.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.meter.readings.service.MeterReadingService;

import io.swagger.annotations.ApiOperation;

@RestController
public class ConsumptionController {
	
	@Autowired
	MeterReadingService meterService;
	
	/**
	 * Returns consumption for meter id and month
	 * 
	 * @param meterId meter id
	 * @param month short month name
	 * @return consumption as BigDecimal value
	 */
	@GetMapping(path="/consumption/{meterId}/{month}")
	@ApiOperation(value="get consumption", notes="returns consumption for a given meter and month")
	public ResponseEntity<BigDecimal> getRadings(@PathVariable String meterId, @PathVariable String month){
		BigDecimal consumption = meterService.getConsumption(meterId, month); 
		if (consumption==null){ 
  			return ResponseEntity.notFound().build();
	    }
		return new ResponseEntity<BigDecimal>(consumption, HttpStatus.OK);  
	}

}
