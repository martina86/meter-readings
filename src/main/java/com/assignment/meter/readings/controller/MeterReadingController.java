package com.assignment.meter.readings.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.meter.readings.entitity.MeterReading;
import com.assignment.meter.readings.exception.MeterException;
import com.assignment.meter.readings.request.MeterReadings;
import com.assignment.meter.readings.response.ErrorResponse;
import com.assignment.meter.readings.response.RestResponse;
import com.assignment.meter.readings.service.MeterReadingService;
import com.assignment.meter.readings.validator.ReadingsValidator;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
 
/**
 *  REST controller with CRUD methods 
 *  for meter readings
 */
@RestController
public class MeterReadingController {

	@Autowired
	MeterReadingService meterService;	
	@Autowired
	ReadingsValidator validator;	 
	
	/**
	 * Returns all readings for one meter	 
	 * @param meterId meter id
	 * @return ResponseEntity with meter readings as body
	 */
	@GetMapping(path = "/readings/{meterId}")
	@ApiOperation(value="Returns all readings for given meter ID", notes="Returns all readings for given meter ID")
	public ResponseEntity<List<MeterReading>> getRadings(@PathVariable String meterId) {		
		List<MeterReading> readings = meterService.get(meterId);
		if (readings.isEmpty()){ 
			return ResponseEntity.notFound().build();
	    }		
		return ResponseEntity.ok().body(readings);  
	}	
	
	/**
	 * Returns readigs for all meters
	 * @return ResponseEntity with meter readings as body
	 */
	@GetMapping(path = "/readings")
	@ApiOperation(value="Get all readings", notes="Returns all readings for any meter ID")
	public ResponseEntity<List<MeterReading>> getRadings() {		
		List<MeterReading> readings = meterService.getAll(); 		
		if (readings.isEmpty()){ 
			return ResponseEntity.notFound().build();
	    }		
		return ResponseEntity.ok().body(readings); 
	}
	
	/**
	 * Creates meter readings for multiple meters in any order
	 * 
	 * @param readings meter readings
	 * @param result BindingResult
	 * @return ResponseEntity with custom MeterReading response
	 */
	@PostMapping(path = "/readings")
    @ApiResponse(code = 207, message="Multi status")
	@ApiOperation(value="Create meter readings", notes="Saves readings for meter with valid readings")
	public ResponseEntity<RestResponse> createReadings(@RequestBody @NotNull MeterReadings readings, BindingResult result) {	
		
		RestResponse response = new RestResponse(new ArrayList<String>(), new ArrayList<String>(), new ArrayList<ErrorResponse>());
		
		validator.validate(readings, result);  
 		response.getErrors().addAll(meterService.mapFieldErrorsToErrorResponse(result));
	
 		//Grouped by meter Id
		Map<String, List<MeterReading>> groupedReadings = readings.getMeterReadings().stream().collect(Collectors.groupingBy(MeterReading::getMeterId)); 
		 		
		groupedReadings.forEach((meterId,readingsList)->{  			
			if (meterService.hasErrors(result.getGlobalErrors(), response.getFailure(), meterId)) {
				try {
					meterService.save(readingsList, result); // business validation 
					response.getSuccess().add("Meter id: " + meterId);
				} catch (MeterException e) { 
					response.getFailure().add("Meter id: " + meterId);
					response.getErrors().add(new ErrorResponse(e.getMessage()));
				}
			} 			
 		});
		//There are no errors, HTTP 201           
		if(response.getErrors().isEmpty()) { 
			return new ResponseEntity<RestResponse>(response, HttpStatus.CREATED);			
 		}                                               
		//There are errors, but some ratios were successfully created, HTTP 207
		else if (!response.getSuccess().isEmpty()) {
			return new ResponseEntity<RestResponse>(response, HttpStatus.MULTI_STATUS);	
		}
		//There are errors, no ratios were successfully created, HTTP 400
		return new ResponseEntity<RestResponse>(response, HttpStatus.BAD_REQUEST);
	}
	
	/**
	 * Updates meter readings for meter id 
	 * Readings in request should be for one meter only
	 * 
	 * @param meterId meter id
	 * @param readings meter readings
	 * @param result BindingResult
	 * @return 
	 */
	@PutMapping(path = "/readings/{meterId}")
	@ApiOperation(value="Update meter readings for one meter ID", notes="For meterID updates all readings")
	public ResponseEntity<RestResponse> update(@NotNull @PathVariable("meterId") String meterId, @RequestBody MeterReadings readings, BindingResult result) {
 
		RestResponse response = new RestResponse(new ArrayList<String>(), new ArrayList<String>(), new ArrayList<ErrorResponse>());
		//Grouped by meter Id
		Map<String, List<MeterReading>> groupedReadings = readings.getMeterReadings().stream().collect(Collectors.groupingBy(MeterReading::getMeterId)); 

		validator.validate(readings, result);
		
		if(result.hasErrors()) {
	 		response.getFailure().add("Meter id: " + meterId);
			response.getErrors().addAll(meterService.mapFieldErrorsToErrorResponse(result));
			return ResponseEntity.badRequest().body(response);			
	 	}
	 	
		try {
			meterService.update(meterId, groupedReadings.get(meterId), result);
		} 
		catch (MeterException e) {
	 		response.getFailure().add("Meter id: " + meterId);
			response.getErrors().add(new ErrorResponse(e.getMessage()));
			return ResponseEntity.badRequest().body(response);			
		}  	 	
	 	
		response.getSuccess().add("Meter id: " + meterId); 
		return ResponseEntity.ok().body(response); 
	}
 	
	/**
	 * Deletes all readings for a meter
	 * @param meterId meter id
	 * @return ResponseEntity with no body
	 */
	@DeleteMapping("/readings/{meterId}")
	@ApiOperation(value="Delete meter readings", notes="Deletes all readings for given meter ID")
	public ResponseEntity<Object> delete(@PathVariable("meterId") String meterId) {		
		List<MeterReading> meterReadings = meterService.get(meterId); 		
		if (meterReadings.isEmpty()) {
  			return ResponseEntity.notFound().build();
		}		
		meterService.delete(meterId);		 
		return ResponseEntity.ok().build();	
	}  
}
