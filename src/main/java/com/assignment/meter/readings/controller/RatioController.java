package com.assignment.meter.readings.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.meter.readings.entitity.Ratio;
import com.assignment.meter.readings.exception.RatioException;
import com.assignment.meter.readings.request.Ratios;
import com.assignment.meter.readings.response.ErrorResponse;
import com.assignment.meter.readings.response.RestResponse;
import com.assignment.meter.readings.service.impl.RatioServiceImpl;
import com.assignment.meter.readings.validator.RatiosValidator;

import io.swagger.annotations.ApiOperation;

/**
 *  REST controller - CRUD   
 *  for ratios and profiles
 */
@RestController
public class RatioController {

	@Autowired
	RatioServiceImpl ratioService;
	
	@Autowired
	RatiosValidator validator; 
 
	/**
	 * Returns all ratios for a profile
	 * 
	 * @param profile profile name
	 * @return ResponseEntity - body ratios 
	 */
	@GetMapping(path="/ratios/{profile}" )
	@ApiOperation(value="Get ratios for profile", notes="Retrieves all ratio values for given profile")
	public ResponseEntity<List<Ratio>> getRatio(@PathVariable String profile){		
		List<Ratio> ratios = ratioService.get(profile);		
		if (ratios.isEmpty()){ 
  			return ResponseEntity.notFound().build();
	    }		
		return ResponseEntity.ok().body(ratios);
	} 
	
	/**
	 * Returns all ratios for all profiles
	 * 
	 * @return ResponseEntity - body 
	 */
	@GetMapping(path="/ratios")
	@ApiOperation(value="Get ratios", notes="Get all ratios")
	public ResponseEntity<List<Ratio>> getRatios(){		
		List<Ratio> ratios = ratioService.getAll();		
		if (ratios.isEmpty()){ 
  			return ResponseEntity.notFound().build();
	    }		
		return ResponseEntity.ok().body(ratios);
	} 	
 
	/**
	 * Deletes all ratios for a profile
	 * 
	 * @param profile profile name
	 * @return ResponseEntity with no body
	 */
	@DeleteMapping("/ratios/{profile}")
 	@ApiOperation(value="Delete ratios", notes="Delete all ratios for a profile")
	public ResponseEntity<Object> delete(@PathVariable String profile) {		
		List<Ratio> ratios = ratioService.get(profile);
  		if (ratios.isEmpty()){ 
  			return ResponseEntity.notFound().build();
		}
		ratioService.delete(profile);
		return ResponseEntity.ok().build();
	}
	
	/**
	 * Updates ratios for profile
	 * Ratios in request should be for one profile only
	 * 
	 * @param profile profile name
	 * @param ratios ratio values for one profile
	 * @param result BindingResult
	 * @return RestResponse
	 */
	@PutMapping(path = "/ratios/{profile}")
	@ApiOperation(value="Update ratios", notes="Update ratios for one profile")
	public ResponseEntity<RestResponse> update(@PathVariable("profile") String profile, @RequestBody Ratios ratios, BindingResult result) {
		
		RestResponse response = new RestResponse(new ArrayList<String>(), new ArrayList<String>(), new ArrayList<ErrorResponse>());
		//Grouped by profile name
		Map<String, List<Ratio>> groupedRatios = ratios.getRatioList().stream().collect(Collectors.groupingBy(Ratio::getProfile)); 
 		
	 	validator.validate(ratios, result);  
	 	
	 	if(result.hasErrors()) {
	 		response.getFailure().add("Profile: " + profile);
			response.getErrors().addAll(ratioService.mapFieldErrorsToErrorResponse(result));
			return ResponseEntity.badRequest().body(response);			
	 	}
	 	
		try {
			ratioService.update(profile, groupedRatios.get(profile), result);
		} catch (RatioException e) {
			response.getFailure().add("Profile: " + profile);
			response.getErrors().add(new ErrorResponse(e.getMessage()));
			return ResponseEntity.badRequest().body(response);			
		}  	 	
	 	
		response.getSuccess().add("Profile: " + profile); 
		return ResponseEntity.ok().body(response);
	}
	

	/**
	 * Creates ratios for multiple profiles in any order
	 * @param ratios
	 * @param result
	 * @returns RestResponse
	 */
	@PostMapping(path="/ratios")
	@ApiOperation(value="Create profiles and ratios", notes="Saves profiles with valid data") 	 
	public ResponseEntity<RestResponse> createRatio(@RequestBody Ratios ratios, BindingResult result) {  
 		
		RestResponse response = new RestResponse(new ArrayList<String>(), new ArrayList<String>(), new ArrayList<ErrorResponse>());
		validator.validate(ratios, result);  //field validation 		
		response.getErrors().addAll(ratioService.mapFieldErrorsToErrorResponse(result));
		 		
		//Grouped by profile name
		Map<String, List<Ratio>> groupedRatios = ratios.getRatioList().stream().collect(Collectors.groupingBy(Ratio::getProfile)); 
		
		groupedRatios.forEach((profile, ratioList)->{	
			if (ratioService.hasErrors(result.getGlobalErrors(), response.getFailure(), profile)) {
				try {
					ratioService.save(ratioList, result); 
					response.getSuccess().add("Profile: " + profile);
				} catch (RatioException e) { 
					response.getFailure().add("Profile: " + profile);
					response.getErrors().add(new ErrorResponse(e.getMessage()));
				}
			}
 		});
 
		//There are no errors, HTTP 201           
		if(response.getErrors().isEmpty()) { 
			return new ResponseEntity<RestResponse>(response, HttpStatus.CREATED);			
 		}                                               
		//There are errors, but some ratios were successfully created, HTTP 207
		else if (!response.getSuccess().isEmpty()) {
			return new ResponseEntity<RestResponse>(response, HttpStatus.MULTI_STATUS);	
		}
		//There are errors, no ratios were successfully created, HTTP 400
		return new ResponseEntity<RestResponse>(response, HttpStatus.BAD_REQUEST);
	}
	 
}
