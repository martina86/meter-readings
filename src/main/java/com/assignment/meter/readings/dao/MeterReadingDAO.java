package com.assignment.meter.readings.dao;

import java.math.BigDecimal;
import java.util.List;

import com.assignment.meter.readings.entitity.MeterReading;

/**
 * DAO methods for meter readings
 *
 */
public interface MeterReadingDAO {
	
	/**
	 * Inserts meter reading
	 * @param meterReading
	 */
	public void insert(MeterReading meterReading);
	
	/**
	 * Returns all meter readings
	 * @return
	 */
	public List<MeterReading> selectAll();
	
	/**
	 * Returns readings for one meter
	 * @param meterId meter id
	 * @return list of meter readings
	 */
	public List<MeterReading> select(String meterId);
	
	/**
	 * Returns meter readings for profile
	 * @param profile profile name
	 * @return list of meter readings
	 */
	public List<MeterReading> selectForProfile(String profile);	
	
	/**
	 * Returns consumption for a meter and a month
	 * @param meterId meter id 
	 * @param month month 
	 * @return consumption value
	 */
	public BigDecimal getConsumption(String meterId, String month) ;
	
	/**
	 * Updates meter reading
	 * @param meterReading
	 */
	public void update(MeterReading meterReading);
	
	/**
	 * Deletes meter reading
	 * @param meterId
	 */
	public void delete(String meterId);
}
