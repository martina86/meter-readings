package com.assignment.meter.readings.dao;

import java.util.List;

import com.assignment.meter.readings.entitity.Ratio;

public interface RatioDAO {
	
 	/**
 	 * Returns ratios for profile
 	 * @param profile profile name
 	 * @return list of ratios
 	 */	
 	public List<Ratio> get(String profile);
 	
 	/**
 	 * Returns ratio for one month and profile
 	 * @param profile profile name
 	 * @param month month
 	 * @return ratio
 	 */
 	public Ratio get(String profile, String month);
	 
	/**
	 * Returns all ratios for all profiles
	 * @return list of ratios
	 */
	public List<Ratio> getAll();
		 
	/**
	 * Insert ratio 
	 * @param ratio
	 */
	public void insert(Ratio ratio);
	
	/**
	 * Returns distinct profile names
	 * @return list of profile names
	 */
	public List<String> getProfiles();
	
	/**
	 * Deletes all ratios for profile
	 * @param profile profile name
	 */
	public void delete(String profile);
	
	/**
	 * Updates ratio value for profile and months
	 * @param ratio
	 */
	public void update(Ratio ratio);
}
