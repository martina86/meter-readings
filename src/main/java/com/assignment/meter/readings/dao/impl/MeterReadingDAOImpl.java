package com.assignment.meter.readings.dao.impl;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.assignment.meter.readings.dao.MeterReadingDAO;
import com.assignment.meter.readings.entitity.MeterReading;

@Repository
@Transactional
public class MeterReadingDAOImpl implements MeterReadingDAO{

	@PersistenceContext
	private EntityManager entityManager;
	
	public void insert(MeterReading meterReading) {
		entityManager.persist(meterReading);		
	}

	@SuppressWarnings("unchecked")
	public List<MeterReading> selectAll() {
		return entityManager.createQuery("from MeterReading").getResultList(); 
	}
	
	@SuppressWarnings("unchecked")
	public List<MeterReading> select(String meterId) {
		return entityManager.createQuery("from MeterReading where meterId = :meterId").setParameter("meterId", meterId).getResultList();
	}
	@SuppressWarnings("unchecked")
	public List<MeterReading> selectForProfile(String profile) {
		return entityManager.createQuery("from MeterReading where profile = :profile").setParameter("profile", profile).getResultList();
	}
	
	public BigDecimal getConsumption(String meterId, String month) {
 		try {
 			return entityManager.createQuery(("select consumption from MeterReading where meterId = :meterId and month = :month"), BigDecimal.class).setParameter("meterId", meterId).setParameter("month", month).getSingleResult();
 		} catch (NoResultException e) {
			return null;
		}
	}
	
	public void update(MeterReading meterReading) {
		entityManager.createQuery("update MeterReading set reading =:reading, consumption=:consumption where profile=:profile and month=:month and meterId=:meterId")
		.setParameter("reading", meterReading.getReading())
		.setParameter("consumption", meterReading.getConsumption())
		.setParameter("profile", meterReading.getProfile())
		.setParameter("month", meterReading.getMonth())
		.setParameter("meterId", meterReading.getMeterId())
		.executeUpdate();	
	}	
	
	public void delete(String meterId) { 
		entityManager.createQuery("delete from MeterReading where meterId = :meterId").setParameter("meterId", meterId).executeUpdate();
	}	
}
