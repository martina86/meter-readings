package com.assignment.meter.readings.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.assignment.meter.readings.dao.RatioDAO;
import com.assignment.meter.readings.entitity.Ratio;

@Repository
@Transactional
public class RatioDAOImpl implements RatioDAO {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	public List<Ratio> get(String profile) {
		return entityManager.createQuery("from Ratio where profile=:profile").setParameter("profile", profile).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public Ratio get(String profile, String month) {
		List<Ratio> ratios = entityManager.createQuery("from Ratio where profile=:profile and month = :month").setParameter("profile", profile).setParameter("month", month).getResultList();
		if (ratios.isEmpty()) {			
			return null;
		}
		else 
			return ratios.get(0);		 
	}
	 
	public List<Ratio> getAll() {
		List<Ratio> ratios =  entityManager.createQuery(("from Ratio"),  Ratio.class).getResultList();
		return ratios;
	}
		 
	public void insert(Ratio ratio) {
		entityManager.persist(ratio);	
	}
	
	public List<String> getProfiles() {
		return entityManager.createQuery(("select distinct(profile) from Ratio"), String.class).getResultList();
	}
	 
	public void delete(String profile) {
		entityManager.createQuery("delete from MeterReading where profile=:profile ").setParameter("profile", profile).executeUpdate(); 
		entityManager.createQuery("delete from Ratio where profile=:profile ").setParameter("profile", profile).executeUpdate();
	}	
	public void update(Ratio ratio) {
		entityManager.createQuery("update Ratio set ratio =:ratio where profile=:profile and month=:month")
		.setParameter("ratio", ratio.getRatio() )
		.setParameter("profile", ratio.getProfile())
		.setParameter("month", ratio.getMonth())
		.executeUpdate();
	}
	
}
