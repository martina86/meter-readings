package com.assignment.meter.readings.entitity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.Valid;

import com.assignment.meter.readings.enums.MonthEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;

@Entity 
@ApiModel(description="Meter readings")
@Valid
public class MeterReading implements Serializable{ 
	
	private static final long serialVersionUID = 1L;
	
	@Id
	private String meterId; //it should be Integer
	@Id
	private String profile;
	@Id
	private String month; 		
	private BigDecimal reading; 
    @JsonIgnore
	private BigDecimal consumption;    
    
	public MeterReading() {
		super();
	}
	public MeterReading(String meterId, String profile, String month, BigDecimal reading) {
		super();
		this.meterId = meterId;
		this.profile = profile;
		this.month = month;
		this.reading = reading;
	}	
	public String getMeterId() {
		return meterId;
	}
	public void setMeterId(String meterId) {
		this.meterId = meterId;
	}
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public BigDecimal getReading() {
		return reading;
	}
	public void setReading(BigDecimal reading) {
		this.reading = reading;
	}	
	public BigDecimal getConsumption() {
		return consumption;
	}
	public void setConsumption(BigDecimal consumption) {
		this.consumption = consumption;
	}
	@Override
	public String toString() {
		return "MeterReading [meterId=" + meterId + ", profile=" + profile + ", month=" + month + ", reading=" + reading
				+ "]";
	}
	@JsonIgnore
	public Integer getMonthValue() {		
		return MonthEnum.get(this.month).getId();
	}
	
}
