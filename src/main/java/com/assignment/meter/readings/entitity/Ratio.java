package com.assignment.meter.readings.entitity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;

@Entity
@ApiModel(description="Ratio for profile") 
public class Ratio implements Serializable{ 
	
	private static final long serialVersionUID = -2799471528988838846L;
	@Id
	private String month; 
	@Id
	private String profile;
	private BigDecimal ratio;
	 
	public Ratio() { 
		super();
	}	
	public Ratio(@NotNull String month, @NotNull String profile, @NotNull BigDecimal ratio) {
		super();
		this.month = month;
		this.profile = profile;
		this.ratio = ratio;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
	public BigDecimal getRatio() {
		return ratio;
	}
	public void setRatio(BigDecimal ratio) {
		this.ratio = ratio;
	}		
}
