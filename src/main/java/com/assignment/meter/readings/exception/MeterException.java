package com.assignment.meter.readings.exception;

public class MeterException extends Exception{ 
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3058771756252369885L;
	
	public final String message;

	public MeterException(String message) {
		this.message=message;
	}
	
	@Override
	public String getMessage(){
        return message;
	}
}
