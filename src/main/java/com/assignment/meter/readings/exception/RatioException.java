package com.assignment.meter.readings.exception;

public class RatioException extends Exception{

 	/**
	 * 
	 */
	private static final long serialVersionUID = -2156451317715778412L;
	public final String message;

	public RatioException(String message) {
		this.message=message;
	}
	
	@Override
	public String getMessage(){
        return message;
	}

}
