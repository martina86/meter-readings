package com.assignment.meter.readings.request;

import java.util.List;

import com.assignment.meter.readings.entitity.MeterReading;

public class MeterReadings {
	private List<MeterReading> meterReadings;

	public List<MeterReading> getMeterReadings() {
		return meterReadings;
	}

	public void setMeterReadings(List<MeterReading> readingsList) {
		this.meterReadings = readingsList;
	}
 
}
