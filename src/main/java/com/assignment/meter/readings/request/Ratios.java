package com.assignment.meter.readings.request;

import java.util.List;

import com.assignment.meter.readings.entitity.Ratio;

public class Ratios {

	private List<Ratio> ratioList;

	public List<Ratio> getRatioList() {
		return ratioList;
	}

	public void setRatioList(List<Ratio> ratioList) {
		this.ratioList = ratioList;
	}
	
	
}
