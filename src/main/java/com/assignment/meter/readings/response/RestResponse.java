package com.assignment.meter.readings.response;

import java.util.List;

/**
 *  Custom response for create and update
 *  controller methods
 */
public class RestResponse {
	
	private List<String> success;	
	private List<String> failure;	 
	private List<ErrorResponse> errors;	 
 
	public RestResponse(List<String> success, List<String> failure, List<ErrorResponse> errors) {
		super();
		this.success = success;
		this.failure = failure;
		this.errors = errors;
	}
	public List<String> getSuccess() {
		return success;
	}
	public void setSuccess(List<String> success) {
		this.success = success;
	}
	public List<String> getFailure() {
		return failure;
	}
	public void setFailure(List<String> failure) {
		this.failure = failure;
	}
	public List<ErrorResponse> getErrors() {
		return errors;
	}
	public void setErrors(List<ErrorResponse> errors) {
		this.errors = errors;
	}
 
}
