package com.assignment.meter.readings.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

import com.assignment.meter.readings.entitity.MeterReading;
import com.assignment.meter.readings.exception.MeterException;
import com.assignment.meter.readings.response.ErrorResponse;

public interface MeterReadingService {
	
	public void save(List<MeterReading> meterReading);
	
	/**
	 * ReadingsList should be in correct order
	 * @param readingsList
	 * @return
	 */
	public BigDecimal getTotalConsumption(List<MeterReading> readingsList) ;
	
	public void save(MeterReading meterReading) ;
	
	public List<MeterReading> get(String meterId) ;
	
	public List<MeterReading> getForProfile(String profile);
	
	public List<MeterReading> getAll() ;
	
	public BigDecimal getConsumption(String meterId, String month);
	
	public void update(List<MeterReading> readingsList);
	
	public void delete(String meterId);
	
	public void save(List<MeterReading> readingsList, Errors errors)  throws MeterException;
	
	public void update(String meterId, List<MeterReading> readingsList, Errors errors)  throws MeterException;
	
	public List<ErrorResponse> mapFieldErrorsToErrorResponse(BindingResult result);
	
	public Boolean hasErrors(List<ObjectError> errors, List<String> fail, String meterId);
}
