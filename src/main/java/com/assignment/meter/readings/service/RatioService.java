package com.assignment.meter.readings.service;

import java.util.List;

import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

import com.assignment.meter.readings.entitity.Ratio;
import com.assignment.meter.readings.exception.RatioException;
import com.assignment.meter.readings.response.ErrorResponse;

public interface RatioService {
	
	public void saveProfile(Ratio ratio);
	
	public void save(List<Ratio> ratios);
	
	public List<Ratio> get(String profile);
	
	public List<Ratio> getAll();
	
	public void update(List<Ratio> ratios);
	
	public void delete(String profile); 
	
	public void save(List<Ratio> ratios, Errors errors)  throws RatioException;
	
	public void update(String profile, List<Ratio> ratios, Errors errors)  throws RatioException;
	
	public List<ErrorResponse> mapFieldErrorsToErrorResponse(BindingResult result);
	
	public Boolean hasErrors(List<ObjectError> errors, List<String> fail, String profile);
}
