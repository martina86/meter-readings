package com.assignment.meter.readings.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

import com.assignment.meter.readings.dao.impl.MeterReadingDAOImpl;
import com.assignment.meter.readings.dao.impl.RatioDAOImpl;
import com.assignment.meter.readings.entitity.MeterReading;
import com.assignment.meter.readings.exception.MeterException;
import com.assignment.meter.readings.response.ErrorResponse;
import com.assignment.meter.readings.service.MeterReadingService;

@Service
public class MeterReadingServiceImpl implements MeterReadingService{
	
	@Autowired
	MeterReadingDAOImpl meterDAO;
	@Autowired
	RatioDAOImpl ratioDAO;	
	@Autowired
	MeterReadingService meterService;
	
	@Autowired
	ResourceBundleMessageSource messageSource;
	public void save(List<MeterReading> meterReading) {
		BigDecimal previousReading= new BigDecimal(0); 
		meterReading.sort(Comparator.comparing(MeterReading::getMonthValue));	
		for (MeterReading reading : meterReading)  {			
			reading.setConsumption(reading.getReading().subtract(previousReading));
			meterDAO.insert(reading);
			previousReading=reading.getReading();
		} 
	}
	
	public BigDecimal getTotalConsumption(List<MeterReading> readingsList) {
		BigDecimal totalConsumption = new BigDecimal(0);
		BigDecimal previousReading = new BigDecimal(0); 
		for (MeterReading meterReading:readingsList) {
			totalConsumption = totalConsumption.add(meterReading.getReading().subtract(previousReading));
			previousReading=meterReading.getReading();
		}
		return totalConsumption;
	}
	
	public void save(MeterReading meterReading) {	
		meterDAO.insert(meterReading);	
	}			
	
	public List<MeterReading> get(String meterId) {
		return meterDAO.select(meterId);
	}
	
	public List<MeterReading> getForProfile(String profile){
		return meterDAO.select(profile);
	}	
	
	public List<MeterReading> getAll() {
		return meterDAO.selectAll();
	}
	
	public BigDecimal getConsumption(String meterId, String month) {
		return  meterDAO.getConsumption(meterId, month); 
	}	
	
	public void update(List<MeterReading> readingsList) {
		BigDecimal previousReading= new BigDecimal(0); 
		readingsList.sort(Comparator.comparing(MeterReading::getMonthValue));	
		for (MeterReading reading : readingsList)  {
			reading.setConsumption(reading.getReading().subtract(previousReading));
			meterDAO.update(reading);
			previousReading=reading.getReading();
		} 
	}
	
	public void delete(String meterId) {
		meterDAO.delete(meterId);
	}
 
	@Override	
	public void save(List<MeterReading> readingsList, Errors errors) throws MeterException {
		String meterId = readingsList.get(0).getMeterId();//assume no missing data 
		if(meterService.get(meterId).isEmpty()) { 
			List<String> profiles = ratioDAO.getProfiles();
			if(profiles.stream().anyMatch(profile->readingsList.get(0).getProfile().equals(profile))){
	    		save(readingsList);  
			}
			else {
				throw new MeterException("Unknown profile name: "+meterId); //+ profilke?
			}	 
		}
		else {		 
			throw new MeterException("Meter already in database: "+meterId);
		} 
	}

	@Override
	public List<ErrorResponse> mapFieldErrorsToErrorResponse(BindingResult result) {
		List<ErrorResponse> errorResponse = new ArrayList<>();
		result.getFieldErrors().forEach(e->{
			ErrorResponse error = new ErrorResponse(
					messageSource.getMessage(e.getCode(),e.getArguments(), Locale.getDefault()),
					e.getField(),
					e.getRejectedValue());	  		 
			errorResponse.add(error);});
		return errorResponse;
	}

	
	@Override	 
	public Boolean hasErrors(List<ObjectError> errors, List<String> fail, String meterId) {
		if( errors.stream().anyMatch(item -> meterId.equals(item.getArguments() == null ? "" : item.getArguments()[0]))) {
			fail.add("Meter id: " + meterId);  
			return false;
		}
		 return true;
	}

	@Override
	public void update(String meterId, List<MeterReading> readingsList, Errors errors) throws MeterException {
		 
		//there should be only one meter ID and key should be same as path variable meter ID 
		if(readingsList==null) {
			throw new MeterException("Path variable 'meterId' does not match meterId in request body");		}
				
		//check if resource exists
 		if (get(meterId).isEmpty()) {
			throw new MeterException("There is no meterId in database to update"); 
		}
 		
		update(readingsList);
	}
 
}
