package com.assignment.meter.readings.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

import com.assignment.meter.readings.dao.impl.RatioDAOImpl;
import com.assignment.meter.readings.entitity.Ratio;
import com.assignment.meter.readings.exception.RatioException;
import com.assignment.meter.readings.response.ErrorResponse;
import com.assignment.meter.readings.service.MeterReadingService;
import com.assignment.meter.readings.service.RatioService;
@Service
public class RatioServiceImpl implements RatioService{
	
	@Autowired
	RatioDAOImpl ratioDao;
	
	@Autowired
	MeterReadingService meterReadingService; 
	
	@Autowired
	ResourceBundleMessageSource messageSource;
	
	public void saveProfile(Ratio ratio) {
		 ratioDao.insert(ratio);		 
	}
	
	public void save(List<Ratio> ratios) {		 
 		for(Ratio ratio : ratios) {
			ratioDao.insert(ratio);
		}
	}
	
	public List<Ratio> get(String profile) {
		return ratioDao.get(profile);
	}
	
	public List<Ratio> getAll() {
		return ratioDao.getAll();
	}
	
	public void update(List<Ratio> ratios) {
		for(Ratio ratio : ratios) {
			ratioDao.update(ratio);
		}
	}
	
	public void delete(String profile) {
		ratioDao.delete(profile); 
	}

	@Override	
	public void save(List<Ratio> ratios, Errors errors) throws RatioException {
		
		String profile = ratios.get(0).getProfile();//assume no missing data 
 
		if(get(ratios.get(0).getProfile()).isEmpty()) {
			BigDecimal totalRatio = ratios.stream().map(Ratio::getRatio).reduce(BigDecimal::add).get();
			if (totalRatio.compareTo(new BigDecimal(1))>0) {
 				throw new RatioException("Invalid total ratio for profile: "+ profile);
			} 
    		save(ratios);  
		}
		else {		 
			throw new RatioException("Profile already in database: "+ratios.get(0).getProfile());
		} 
	}

	@Override
	public List<ErrorResponse> mapFieldErrorsToErrorResponse(BindingResult result) {
		List<ErrorResponse> errorResponse = new ArrayList<>();
		result.getFieldErrors().forEach(e->{
			ErrorResponse error = new ErrorResponse(
					messageSource.getMessage(e.getCode(),e.getArguments(), Locale.getDefault()),
					e.getField(),
					e.getRejectedValue());	  		 
			errorResponse.add(error);});
		return errorResponse;
	}

	
	@Override	 
	public Boolean hasErrors(List<ObjectError> errors, List<String> fail, String profile) {
		if( errors.stream().anyMatch(item -> profile.equals(item.getArguments() == null ? "" : item.getArguments()[0]))) {
			fail.add("Profile: " + profile);
			return false;
		}
		 return true;
	}

	@Override
	public void update(String profile, List<Ratio> ratios, Errors errors) throws RatioException {
		 
		//there should be only one meter ID and key should be same as path variable meter ID 
		if(ratios==null) {
			throw new RatioException("Path variable 'profile' does not match profile in request body");		}
				
		//check if resource exists
 		if (get(profile).isEmpty()) {
			throw new RatioException("There is no profile in database to update"); 
		}
 		//check if there are meter readings for profile
		if (!meterReadingService.getForProfile(profile).isEmpty()) {
			throw new RatioException("Unable to update, there are meter readings for profile: "+ profile); 
		}
		
		update(ratios);
	}
}
