package com.assignment.meter.readings.validator;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.assignment.meter.readings.entitity.Ratio;
import com.assignment.meter.readings.enums.MonthEnum;
import com.assignment.meter.readings.request.Ratios;

/**
  * Assumptions:
 * 	- there is no specific order for the rows
 *  - there is only one value per month-profile combination
 *  - assume no missing data (input data always contains 12 months per profile)
 */
@Component
public class RatiosValidator{
 
	public boolean supports(Class<?> clazz) {		 
		return Ratio.class.equals(clazz);  
	}

	/**
	 * Ratio validation
	 * @param target Ratios
	 * @param errors Errors
	 */
	public void validate(Object target, Errors errors) { 
 		Ratios ratios = (Ratios) target;  	  
 		List<Ratio> ratioList = ratios.getRatioList();
		Map<String, List<Ratio>> groupedRatios = ratioList.stream().collect(Collectors.groupingBy(Ratio::getProfile)); 
		groupedRatios.forEach((profile, group)->{    
			for (Ratio ratio : group) {
	 			if (ratio.getRatio().compareTo(new BigDecimal(0))<0 || ratio.getRatio().compareTo(new BigDecimal(1))>0) {
 	 	  			errors.rejectValue("ratioList["+ratioList.indexOf(ratio)+"].ratio","error.profile", new Object[] {profile, ratio.getMonth(), ratio.getRatio()}, "Invalid ratio value");
 	 	  			errors.reject("Invalid profile", new Object[] {profile}, "Invalid profile");
	 			}
	 			if (MonthEnum.get(ratio.getMonth())==null){ 
 	 	  			errors.reject("Invalid profile", new Object[] {profile}, "Invalid profile");
					errors.rejectValue("ratioList["+ratioList.indexOf(ratio)+"].month", "error.month.name", new Object[] {profile, ratio.getMonth(), ratio.getRatio()}, "Incorrect month name"); 
	 			}	 			
	 		}  
		});		
	}
}


