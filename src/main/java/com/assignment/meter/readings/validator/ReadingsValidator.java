package com.assignment.meter.readings.validator;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.assignment.meter.readings.dao.impl.RatioDAOImpl;
import com.assignment.meter.readings.entitity.MeterReading;
import com.assignment.meter.readings.entitity.Ratio;
import com.assignment.meter.readings.request.MeterReadings;
import com.assignment.meter.readings.service.impl.MeterReadingServiceImpl;
 

/**
 * Assignment assumptions:
 *  - there is no specific order for the rows
 *  - there is only one value per month-profile combination
 *  - meterID is unique 
 *  - no missing data
 */
@Component
public class ReadingsValidator implements org.springframework.validation.Validator {

	Logger logger = LoggerFactory.getLogger(ReadingsValidator.class);
	 
	@Autowired
	RatioDAOImpl ratioDAO;
	
	@Autowired
	MeterReadingServiceImpl meterService;
	
	@Override
	public boolean supports(Class<?> clazz) {
        return MeterReading.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		 
		MeterReadings meterReading = (MeterReadings) target; 	
		List<MeterReading> meterReadings = meterReading.getMeterReadings();
		List<String> profiles = ratioDAO.getProfiles();
		//Grouped by meter Id
		Map<String, List<MeterReading>> groupedReadings = meterReadings.stream().collect(Collectors.groupingBy(MeterReading::getMeterId)); 
		 		
		groupedReadings.forEach((meterId,groupedList)->{
			BigDecimal previousReading = new BigDecimal(0);
			groupedList.sort(Comparator.comparing(MeterReading::getMonthValue));	
			BigDecimal totalConsumption =  meterService.getTotalConsumption(groupedList);
			logger.info("Total consumption for meter id" + meterId +" :" + totalConsumption);
			for (MeterReading reading:groupedList) {
				if(profiles.stream().anyMatch(profile->reading.getProfile().equals(profile))){
					if (reading.getReading().compareTo(previousReading)==-1) {
	 	 	  			errors.reject("Invalid meter", new Object[] {meterId}, "Invalid meter");
	 					errors.rejectValue("meterReadings["+meterReadings.indexOf(reading)+"].reading","error.previous.value", new Object[] {reading.getMeterId(), reading.getMonth(), reading.getReading(), previousReading }, "Invalid previous reading");
					}
					else {
						Ratio ratio = ratioDAO.get(reading.getProfile(), reading.getMonth()); //ovo van i pozovi samo jednom
					 	if(!isInRange(ratio.getRatio(), reading.getReading().subtract(previousReading), totalConsumption)){
		 	 	  			errors.reject("Invalid meter", new Object[] {meterId}, "Invalid meter");
							errors.rejectValue("meterReadings["+meterReadings.indexOf(reading)+"].reading", "error.meter.range", new Object[] {reading.getMeterId(), reading.getReading(), reading.getProfile(), reading.getMonth()}, "Meter reading is not consistent with ratio");	
					 	}
					}
					previousReading=reading.getReading();
				}
				else { 
 	 	  			errors.reject("Invalid meter", new Object[] {meterId}, "Invalid meter");
					errors.rejectValue("meterReadings["+meterReadings.indexOf(reading)+"].profile","error.meter.profile", new Object[] {reading.getMeterId(), reading.getProfile()}, "Unknown profile name"); 
				}
			}		 			
		});
  	} 

	/**
	 * Checks if meter reading is consistent 
	 * with the ratio with a tolerance of 25%
	 * @param ratio
	 * @param consumption
	 * @param totalConsumption
	 * @return
	 */
	private boolean isInRange(BigDecimal ratio, BigDecimal consumption, BigDecimal totalConsumption) {
 		BigDecimal ratioConsumption = ratio.multiply(totalConsumption); 
		BigDecimal high = ratioConsumption.add(ratioConsumption.multiply(new BigDecimal(0.25)));
		BigDecimal low = ratioConsumption.subtract(ratioConsumption.multiply(new BigDecimal(0.25)));
		if(consumption.compareTo(high)==1 || consumption.compareTo(low)==-1){
			logger.info("Consumption:" + consumption + ", low :" + low + ", high: " + high );
			return false;			 
		}
		return true;
	}
}
