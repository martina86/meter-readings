package com.assignment.meter.readings.controller;

import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.assignment.meter.readings.dao.impl.RatioDAOImpl;
import com.assignment.meter.readings.entitity.MeterReading;
import com.assignment.meter.readings.service.MeterReadingService;
import com.assignment.meter.readings.validator.ReadingsValidator;
import com.google.gson.Gson;

public class MeterReadingControllerUnitTest {
	private MockMvc mockMvc;

	@Mock
	private MeterReadingService meterService;

	@Mock
	RatioDAOImpl ratioDAO;

	@Mock
	ReadingsValidator validator;

	@InjectMocks
	private MeterReadingController meterReadingController;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(meterReadingController).build();
	}

	@Test
	public void get_by_meter_id_ShouldReturnHttpStatusCode200() throws Exception {
		List<MeterReading> readings = new ArrayList<>();
		readings.add(new MeterReading("0001", "A", "FEB", new BigDecimal(0.05)));
		when(meterService.get("0001")).thenReturn(readings);
		mockMvc.perform(get("/readings/{meterId}", "0001")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$[0].meterId", is("0001"))).andExpect(jsonPath("$[0].profile", is("A")))
				.andExpect(jsonPath("$[0].month", is("FEB")))
				.andExpect(jsonPath("$[0].reading", is(new BigDecimal(0.05))));
	}

	@Test
	public void get_by_meter_id_ShouldReturnHttpStatusCode204() throws Exception {
		mockMvc.perform(get("/readings/{meterId}", "0001")).andExpect(status().isNotFound());
	}

	@Test
	public void delete_ReadingIsFound_ShouldReturnHttpStatusCode200() throws Exception {
		List<MeterReading> readings = new ArrayList<>();
		readings.add(new MeterReading("0001", "A", "FEB", new BigDecimal(0.05)));
		when(meterService.get("0001")).thenReturn(readings);
		mockMvc.perform(delete("/readings/{meterId}", "0001")).andExpect(status().isOk());
	}

	@Test
	public void delete_RatioIsNotFound_ShouldReturnHttpStatusCode404() throws Exception {
		mockMvc.perform(delete("/readings/{meterId}", "0001")).andExpect(status().isNotFound());
	}

	@Test
	public void create_ShouldReturnHttpStatusCode201() throws Exception {
		when(meterService.get("0001")).thenReturn(new ArrayList<>());
		mockMvc.perform(post("/readings").content(new Gson().toJson(getReadings()))
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isCreated());
	}

	private List<MeterReading> getReadings() {
		List<MeterReading> readingsList = new ArrayList<>();
		readingsList.add(new MeterReading("0001", "A", "DEC", new BigDecimal(200)));
		readingsList.add(new MeterReading("0001", "A", "JAN", new BigDecimal(10)));
		readingsList.add(new MeterReading("0001", "A", "FEB", new BigDecimal(20)));
		readingsList.add(new MeterReading("0001", "A", "MAR", new BigDecimal(34)));
		readingsList.add(new MeterReading("0001", "A", "JUN", new BigDecimal(85)));
		readingsList.add(new MeterReading("0001", "A", "NOV", new BigDecimal(180)));
		readingsList.add(new MeterReading("0001", "A", "APR", new BigDecimal(52)));
		readingsList.add(new MeterReading("0001", "A", "MAY", new BigDecimal(74)));
		readingsList.add(new MeterReading("0001", "A", "JUL", new BigDecimal(107)));
		readingsList.add(new MeterReading("0001", "A", "AUG", new BigDecimal(129)));
		readingsList.add(new MeterReading("0001", "A", "OCT", new BigDecimal(160)));
		readingsList.add(new MeterReading("0001", "A", "SEP", new BigDecimal(170)));
		return readingsList;
	}

}
