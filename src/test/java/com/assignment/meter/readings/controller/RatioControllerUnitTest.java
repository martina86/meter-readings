package com.assignment.meter.readings.controller;

import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.assignment.meter.readings.entitity.Ratio;
import com.assignment.meter.readings.request.Ratios;
import com.assignment.meter.readings.service.impl.RatioServiceImpl;
import com.assignment.meter.readings.validator.RatiosValidator;
import com.google.gson.Gson;

public class RatioControllerUnitTest {
	private MockMvc mockMvc;

	@Mock
	private RatioServiceImpl ratioService;

	@Spy
	RatiosValidator validator;

	@InjectMocks
	private RatioController ratioController;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(ratioController).build();
	}

	@Test
	public void get_by_profile_ShouldReturnHttpStatusCode200() throws Exception {
		List<Ratio> ratios = new ArrayList<>();
		Ratio ratio = new Ratio("DEC", "A", new BigDecimal(0.05));
		ratios.add(ratio);
		when(ratioService.get("A")).thenReturn(ratios);
		mockMvc.perform(get("/ratios/{profile}", "A")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$[0].profile", is("A"))).andExpect(jsonPath("$[0].month", is("DEC")))
				.andExpect(jsonPath("$[0].ratio", is(new BigDecimal(0.05))));
	}

	@Test
	public void get_by_profile_ShouldReturnHttpStatusCode404() throws Exception {
		mockMvc.perform(get("/ratios/{profile}", "A")).andExpect(status().isNotFound());
	}

	@Test
	public void delete_RatioIsFound_ShouldReturnHttpStatusCode200() throws Exception {
		List<Ratio> ratios = new ArrayList<>();
		Ratio ratio = new Ratio("DEC", "A", new BigDecimal(0.05));
		ratios.add(ratio);
		when(ratioService.get("A")).thenReturn(ratios);
		mockMvc.perform(delete("/ratios/{profile}", "A")).andExpect(status().isOk());
	}

	@Test
	public void delete_RatioIsNotFound_ShouldReturnHttpStatusCode404() throws Exception {
		mockMvc.perform(delete("/ratios/{profile}", "A")).andExpect(status().isNotFound());
	}

	@Test
	public void create_ShouldReturnHttpStatusCode201() throws Exception {
		mockMvc.perform(post("/ratios").content(new Gson().toJson(getRatioLists()))
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isCreated());
	}

	private Ratios getRatioLists() {
		Ratios ratios = new Ratios();
		List<Ratio> ratioList = new ArrayList<>();
		ratioList.add(new Ratio("DEC", "A", new BigDecimal("0.05")));
		ratioList.add(new Ratio("JAN", "A", new BigDecimal("0.05")));
		ratioList.add(new Ratio("FEB", "A", new BigDecimal("0.05")));
		ratioList.add(new Ratio("MAR", "A", new BigDecimal("0.1")));
		ratioList.add(new Ratio("JUN", "A", new BigDecimal("0.05")));
		ratioList.add(new Ratio("APR", "A", new BigDecimal("0.2")));
		ratioList.add(new Ratio("MAY", "A", new BigDecimal("0.05")));
		ratioList.add(new Ratio("JUL", "A", new BigDecimal("0.05")));
		ratioList.add(new Ratio("AUG", "A", new BigDecimal("0.1")));
		ratioList.add(new Ratio("SEP", "A", new BigDecimal("0.1")));
		ratioList.add(new Ratio("OCT", "A", new BigDecimal("0.1")));
		ratioList.add(new Ratio("NOV", "A", new BigDecimal("0.1")));
		ratios.setRatioList(ratioList);
		return ratios;
	}

}
